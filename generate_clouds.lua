return function(total_clouds, intensity)
    local clouds = {}

    local height_space = HEIGHT / 3
    local width_step = WIDTH / total_clouds
    local peak_variation = 10

    for i=1, total_clouds do
        local cloud_vertices = {}
        local cloud_peaks = math.random(2, 8)

        local cloud_width = math.random(width_step / 3, width_step)
        local cloud_y = math.random(height_space / 10, height_space / 10 * 8)

        table.insert(cloud_vertices, {
            width_step * i - width_step / 2 + cloud_width / 2,
            cloud_y
        })
        table.insert(cloud_vertices, {
            width_step * i - width_step / 2,
            cloud_y + math.random(5, 20)
        })
        table.insert(cloud_vertices, {
            width_step * i - width_step / 2 - cloud_width / 2,
            cloud_y
        })

        local peak_step = cloud_width / cloud_peaks
        local last_position = {
            width_step * i - width_step / 2 - cloud_width / 2,
            cloud_y
        }
        for peak=1, cloud_peaks do
            local random_variation = math.random(0, peak_variation)
            local current_position = {
                (width_step * i - width_step / 2 - cloud_width /
                    2 + peak * peak_step),
                cloud_y - random_variation
            }

            table.insert(cloud_vertices, last_position)
            table.insert(cloud_vertices, {
                (width_step * i - width_step / 2 - cloud_width /
                    2 + peak * peak_step),
                cloud_y
            })
            table.insert(cloud_vertices, current_position)

            table.insert(cloud_vertices, last_position)
            table.insert(cloud_vertices, {
                (width_step * i - width_step / 2 - cloud_width / 2
                    + (peak - 1) * peak_step),
                cloud_y
            })
            table.insert(cloud_vertices, {
                (width_step * i - width_step / 2 - cloud_width /
                    2 + peak * peak_step),
                cloud_y
            })

            last_position = current_position
        end

        table.insert(clouds, {
            color={1,1,1,intensity},
            mesh=love.graphics.newMesh(cloud_vertices, "triangles", "static")
        })

        -- Create a clone black cloud shifted to bottom
        for _, vertice in ipairs(cloud_vertices) do
            vertice[2] = vertice[2] + 7
        end

        table.insert(clouds, {
            color={0,0,0,intensity / 2},
            mesh=love.graphics.newMesh(cloud_vertices, "triangles", "static")
        })
    end

    return clouds
end
