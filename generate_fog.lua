local moonshine = require "moonshine"

-- Prepare effect similar to fog
local effect = moonshine(moonshine.effects.fog)
effect.chain(moonshine.effects.boxblur)
effect.chain(moonshine.effects.filmgrain)

-- Prepare canvas to be used as texture
local canvas = love.graphics.newCanvas(WIDTH, HEIGHT)
love.graphics.setCanvas(canvas)
love.graphics.clear(1, 1, 1, 0)

effect.draw(function()end)

love.graphics.setCanvas()

return function(total_montains, density, start_y)
    -- Create fog layers
    local fogs = {}

    local total_peaks = math.random(WIDTH / 80, WIDTH / 20)
    local width_step = (WIDTH / total_peaks)

    local current_height = start_y
    local height_variation = 20
    local height_step = (HEIGHT - current_height) /
        (total_montains + math.random(1, 5))
    local variation_smoothing = 0.4

    for _=1, total_montains do
        local fog_vertices = {}
        local last_position = {0, current_height, 0, current_height / HEIGHT}

        local last_height_variation = 0
        for peak=1, total_peaks do
            local random_height = math.random(
                -height_variation,
                height_variation
            )
            random_height = (last_height_variation +
                (last_height_variation - random_height) * variation_smoothing)

            local current_position = {
                width_step * peak,
                current_height + random_height,
                (width_step * peak) / WIDTH,
                (current_height + random_height) / HEIGHT
            }

            table.insert(fog_vertices, last_position)
            table.insert(fog_vertices, {
                (peak - 1) * width_step,
                current_height + height_variation,
                ((peak - 1) * width_step) / WIDTH,
                (current_height + height_variation) / HEIGHT
            })
            table.insert(fog_vertices, {
                peak * width_step,
                current_height + height_variation,
                (peak * width_step) / WIDTH,
                (current_height + height_variation) / HEIGHT
            })

            table.insert(fog_vertices, last_position)
            table.insert(fog_vertices, current_position)
            table.insert(fog_vertices, {
                peak * width_step,
                current_height + height_variation,
                (peak * width_step) / WIDTH,
                (current_height + height_variation) / HEIGHT
            })

            last_position = current_position
        end

        -- Rectange in the bottom
        table.insert(fog_vertices,
            {
                0, current_height + height_variation,
                0, (current_height + height_variation) / HEIGHT}
        )
        table.insert(fog_vertices, {0, HEIGHT, 0, 1})
        table.insert(fog_vertices, {
            WIDTH, current_height + height_variation,
            1, (current_height + height_variation) / HEIGHT
        })

        -- Rectange in the bottom
        table.insert(fog_vertices,{
            WIDTH, current_height + height_variation,
            1, (current_height + height_variation) / HEIGHT
        })
        table.insert(fog_vertices, {WIDTH, HEIGHT, 1, 1})
        table.insert(fog_vertices, {
            0, HEIGHT, 0, 1
        })

        local fog_mesh = love.graphics.newMesh(
            fog_vertices,
            "triangles",
            "static"
        )
        fog_mesh:setTexture(canvas)
        current_height = current_height + height_step

        table.insert(fogs, {
            color={1, 1, 1, density},
            mesh=fog_mesh
        })
    end

    return fogs
end
