local WIDTH, HEIGHT, _ = love.window.getMode()

local TOTAL_LAYERS = 11
local BACKGROUND_PATH = "%d_background.png"

local MOVE_SPEED = 0.15
local GRAVITY = 18

local background_layers
local player_position
local y_speed

local platform_y = 100

local function load_backgrounds()
    background_layers = {}
    for i=1, TOTAL_LAYERS do
        local x_offset = 0
        if i == 1 then
            x_offset = -1
        end

        table.insert(background_layers, {
            image=love.graphics.newImage(
                string.format(BACKGROUND_PATH, i)
            ),
            position1={x=WIDTH/2, y=0, x_offset=x_offset},
            position2={x=-WIDTH/2, y=0, x_offset=0}
        })

        background_layers[i].width = background_layers[i].image:getWidth()
    end
end

function love.load()
    player_position = {x=WIDTH / 2, y=HEIGHT - platform_y}
    y_speed = 0

    load_backgrounds()
end

function love.draw()
    love.graphics.clear(1,1,1,1)
    for i, layer in ipairs(background_layers) do
        if i == 1 then
            print(layer.position1.x)
            print(layer.position2.x)
        end

        love.graphics.draw(
            layer.image,
            layer.position1.x + layer.position1.x_offset,
            layer.position1.y
        )
        love.graphics.draw(
            layer.image,
            layer.position2.x + layer.position2.x_offset,
            layer.position2.y
        )
    end

    love.graphics.rectangle(
        "fill",
        player_position.x,
        player_position.y,
        50,
        50
    )
end

function love.update(dt)
    if love.keyboard.isDown("right") then
        for i, layer in ipairs(background_layers) do
            local pos1_old_x = layer.position1.x
            local pos2_old_x = layer.position2.x

            layer.position1.x = math.ceil(layer.position1.x + MOVE_SPEED * i * i)
            layer.position2.x = math.ceil(layer.position2.x + MOVE_SPEED * i * i)

            if pos1_old_x < 0 and layer.position1.x >= 0 then
                layer.position2.x = layer.position1.x - layer.width
                if i == 1 then
                    layer.position1.x_offset = layer.position1.x_offset - 2
                end
            else
                if pos2_old_x < 0 and layer.position2.x >= 0 then
                    layer.position1.x = layer.position2.x - layer.width
                    if i == 1 then
                        layer.position1.x_offset = layer.position1.x_offset + 2
                    end
                end
            end
        end
    elseif love.keyboard.isDown("left") then
        for i, layer in ipairs(background_layers) do
            local pos1_old_x = layer.position1.x
            local pos2_old_x = layer.position2.x

            layer.position1.x = math.floor(layer.position1.x - MOVE_SPEED * i * i)
            layer.position2.x = math.floor(layer.position2.x - MOVE_SPEED * i * i)

            if pos1_old_x > 0 and layer.position1.x <= 0 then
                layer.position2.x = layer.position1.x + layer.width

                if i == 1 then
                    layer.position1.x_offset = layer.position1.x_offset + 2
                end
            else
                if pos2_old_x > 0 and layer.position2.x <= 0 then
                    layer.position1.x = layer.position2.x + layer.width
                    if i == 1 then
                        layer.position1.x_offset = layer.position1.x_offset - 2
                    end
                end
            end
        end
    end

    y_speed = y_speed - GRAVITY * dt

    player_position.y = player_position.y - y_speed
    if player_position.y > HEIGHT - platform_y then
        player_position.y = HEIGHT - platform_y
    end
end

function love.keypressed(key)
    if key == "r" then
        load_backgrounds()
    end

    if key == "up" then
        y_speed = 10
    end
end
