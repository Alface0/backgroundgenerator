return function(start_color, end_color)
    local base_color = {
        start_color[1],
        start_color[2],
        start_color[3],
        start_color[4]
    }
    local diff_step = {
        (end_color[1] - start_color[1]) / HEIGHT,
        (end_color[2] - start_color[2]) / HEIGHT,
        (end_color[3] - start_color[3]) / HEIGHT,
        (end_color[4] - start_color[4]) / HEIGHT
    }
    for y=0, HEIGHT-1 do
        love.graphics.setColor(unpack(base_color))
        for x=0, WIDTH-1 do
            love.graphics.points(x, y)
        end

        -- Update color
        base_color[1] = base_color[1] + diff_step[1]
        base_color[2] = base_color[2] + diff_step[2]
        base_color[3] = base_color[3] + diff_step[3]
        base_color[4] = base_color[4] + diff_step[4]
    end
end
