return function(total_montains, mountains_color, start_y, peaks_count)
    local _mountains = {}

    local current_height = start_y or HEIGHT / 5 * 2
    local height_step = (HEIGHT - current_height) / (total_montains + 4)
    local base_height_variation = 20

    local end_transparency = 0.75
    local transparency_step = (end_transparency - mountains_color[4]) / total_montains

    for i=1, total_montains do
        local height_variation = base_height_variation + i * 4

        local total_peaks = peaks_count + math.random(-6, 6)
        local width_step = WIDTH / total_peaks
        local mountain_vertices = {}

        for peak=1, total_peaks do
            local random_height = math.random(
                -height_variation,
                height_variation
            )

            local last_position
            if #mountain_vertices == 0 then
                last_position = {0, current_height}
            else
                last_position = mountain_vertices[#mountain_vertices - 1]
            end

            table.insert(mountain_vertices, last_position)
            table.insert(mountain_vertices, {
                (peak - 1) * width_step, current_height + height_variation
            })
            table.insert(mountain_vertices, {
                peak * width_step, current_height + height_variation
            })

            table.insert(mountain_vertices, last_position)
            if peak == total_peaks then
                -- Make Last point match the height of the first one
                table.insert(mountain_vertices, {
                    peak * width_step, current_height
                })
            else
                table.insert(mountain_vertices, {
                    peak * width_step, current_height + random_height
                })
            end

            table.insert(mountain_vertices, {
                peak * width_step, current_height + height_variation
            })
        end

        -- Rectange in the bottom
        table.insert(mountain_vertices,
            {0, current_height + height_variation}
        )
        table.insert(mountain_vertices, {0, HEIGHT})
        table.insert(mountain_vertices, {
            WIDTH, current_height + height_variation
        })

        -- Rectange in the bottom
        table.insert(mountain_vertices,
            {WIDTH, current_height + height_variation}
        )
        table.insert(mountain_vertices, {WIDTH, HEIGHT})
        table.insert(mountain_vertices, {
            0, HEIGHT
        })

        table.insert(_mountains , {
            color={
                mountains_color[1],
                mountains_color[2],
                mountains_color[3],
                mountains_color[4] + transparency_step * i
            },
            mesh=love.graphics.newMesh(
                mountain_vertices,
                "triangles",
                "static"
            )
        })

        current_height = current_height + height_step
    end

    return _mountains
end
