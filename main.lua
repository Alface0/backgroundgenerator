WIDTH, HEIGHT, _ = love.window.getMode()

local draw_background = require "draw_background"
local generate_mountains = require "generate_mountains"
local generate_clouds = require "generate_clouds"
local generate_fog = require "generate_fog"

local layers
local mountains
local clouds
local fogs

local background_fog
local foreground_mountain

local current_seed = 1
local total_mountains
local mountain_y

local base_color
local start_color
local end_color

local taking_screenshot = false
local screenshot_layer = 1

local screenshot_canvas

local function load_backgrounds()
    screenshot_layer = 1
    taking_screenshot = false

    layers = {}
    mountains = {}
    clouds = {}
    fogs = {}

    math.randomseed(current_seed)
    total_mountains = math.random(2, 6)
    mountain_y = math.random(HEIGHT / 5, HEIGHT / 5 * 3)

    base_color = {
        math.random(),
        math.random(),
        math.random(),
        1
    }

    start_color = {
        math.random(),
        math.random(),
        math.random(),
        0.8
    }

    end_color = {
        math.random(),
        math.random(),
        math.random(),
        0.8
    }

    mountains = generate_mountains(
        total_mountains,
        {
            base_color[1],
            base_color[2],
            base_color[3],
            0.05
        },
        mountain_y,
        20
    )
    fogs = generate_fog(
        total_mountains,
        0.05,
        mountain_y + (HEIGHT / 5 * 0.2)
    )
    background_fog = generate_fog(
        1,
        0.1,
        mountain_y - (HEIGHT / 5 * 0.8)
    )
    clouds = generate_clouds(
        math.random(3,5),
        0.04
    )
    foreground_mountain = generate_mountains(
        1,
        {
            base_color[1] / 4,
            base_color[2] / 4,
            base_color[3] / 4,
            0.8
        },
        HEIGHT / 5 * 4,
        6
    )

    -- Prepare drawing in layers
    -- Background
    table.insert(layers, function()
        draw_background(
            start_color,
            end_color
        )
    end)


    -- Background fog
    table.insert(layers, function()
        love.graphics.setColor(unpack(background_fog[1].color))
        love.graphics.draw(background_fog[1].mesh)
    end)

    -- clouds
    table.insert(layers, function()
        for _, cloud in ipairs(clouds) do
            love.graphics.setColor(unpack(cloud.color))
            love.graphics.draw(cloud.mesh)
        end
    end)

    -- A layer for each mountain and fog pairs
    for i, mountain in ipairs(mountains) do
        table.insert(layers, function()
            love.graphics.setColor(unpack(mountain.color))
            love.graphics.draw(mountain.mesh)

            love.graphics.setColor(unpack(fogs[i].color))
            love.graphics.draw(fogs[i].mesh)
        end)
    end

    -- Montain close to the bottom
    table.insert(layers, function()
        for _, mountain in ipairs(foreground_mountain) do
            love.graphics.setColor(unpack(mountain.color))
            love.graphics.draw(mountain.mesh)
        end
    end)

    -- Block where the player may step
    table.insert(layers, function()
        love.graphics.setColor(0.3, 0.3, 0.3, 1)
        love.graphics.rectangle("fill", 0, HEIGHT / 5 * 4.6, WIDTH, 200)
    end)
end

function love.load()
    load_backgrounds()

    screenshot_canvas = love.graphics.newCanvas(WIDTH, HEIGHT)
end

function love.draw()
    love.graphics.clear(0,0,0,0)

    if not taking_screenshot then
        for _, layer in ipairs(layers) do
            layer()
        end
    else
        -- Take screenshot of one layer each time
        love.graphics.setCanvas(screenshot_canvas)
        love.graphics.clear(0,0,0,0)
        layers[screenshot_layer]()

        love.graphics.setCanvas()

        -- Output canvas to file
        local image_data = screenshot_canvas:newImageData(nil, nil, 0, 0, WIDTH, HEIGHT)
        image_data:encode("png", screenshot_layer.."_background.png")

        if screenshot_layer == #layers then
            taking_screenshot = false
            screenshot_layer = 1
        end

        screenshot_layer = screenshot_layer + 1
    end
end

function love.keypressed(key, scancode, isrepeat)
    if key == "r" then
        current_seed = current_seed + 1
        print(current_seed)
        load_backgrounds()
    end

    if key == "e" then
        taking_screenshot = true
    end
end
