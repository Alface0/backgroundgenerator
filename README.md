# Procedural Background Generator

This project implements a produceral background generator which exports the background in multiple layers and also runs with a parallax effect.

## Demo

### Backgrounds

![Background Example 1](demo/background_2.png)
![Background Example 2](demo/background_3.png)


### Parallax

![Parallax Example 1](demo/parallax_02.gif)
![Parallax Example 2](demo/parallax_03.gif)


### Full demo

Check the video for a full usage demo - [Procedural Background Generator with Parallax](https://youtu.be/O8ob2DGnroI)

## Usage

To generate backgrounds stay in the base folder and run with [love2d](https://love2d.org/) in version 11.2.
```sh
love .
```

Then while running you can press `r` to change the background seed and `e` to export the background as pngs to the love2d system [folder](https://love2d.org/wiki/love.filesystem).

To run the parallax you will need to have already exported some background. Then go to the paralax folder and run love2d.
```sh
love .
```

Now you can move with the key arrows and if you have exported other background you can press `r` to refresh the parallax.

## Devlog

If you want to see how it was implemented, check the devlog in my website - [drmargarido](https://drmargarido.pt/background_generator)
